#ifndef TCP_PROXY_DEFS_H_
#define TCP_PROXY_DEFS_H_

#include <assert.h>
#include <uv.h>

/*
 * ASSERT() is for debug checks, CHECK() for run-time sanity checks.
 */
#if defined(NDEBUG)
# define ASSERT(exp)
# define CHECK(exp)   do { if (!(exp)) abort(); } while (0)
#else
# define ASSERT(exp)  assert(exp)
# define CHECK(exp)   assert(exp)
#endif

#define UNREACHABLE() CHECK(!"Unreachable code reached.")

/* This macro looks complicated but it's not: it calculates the address
 * of the embedding struct through the address of the embedded struct.
 * In other words, if struct A embeds struct B, then we can obtain
 * the address of A by taking the address of B and subtracting the
 * field offset of B in A.
 */
#define CONTAINER_OF(ptr, type, field) \
  ((type *) ((char *) (ptr) - ((char *) &((type *) 0)->field)))

/* client types */
struct client_ctx;

typedef struct {
    unsigned char rdstate;
    unsigned char wrstate;
    unsigned int idle_timeout;
    struct client_ctx *client;  /* Backlink to owning client context. */
    ssize_t result;
    union {
      uv_handle_t handle;
      uv_stream_t stream;
      uv_tcp_t tcp;
    } handle;
    uv_timer_t timer_handle;  /* For detecting timeouts. */
    uv_write_t write_req;
    /* We only need one of these at a time so make them share memory. */
    union {
      uv_getaddrinfo_t addrinfo_req;
      uv_connect_t connect_req;
      uv_req_t req;
      struct sockaddr_in addr4;
      struct sockaddr addr;
      char buf[2048];  /* Scratch space. Used to read data into. */
    } t;
} conn;

typedef struct client_ctx {
    unsigned int state;
    conn incoming;  /* Connection with the client. */
    conn outgoing;  /* Connection with upstream. */
} client_ctx;

#endif /* TCP_PROXY_DEFS_H_ */
