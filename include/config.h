#ifndef TCP_PROXY_CONFIG_H_
#define TCP_PROXY_CONFIG_H_

#define DEFAULT_PROXY_HOST "127.0.0.1"
#define DEFAULT_PROXY_PORT 80
#define DEFAULT_PROXY_UPSTREAM_HOST "127.0.0.1"
#define DEFAULT_PROXY_UPSTREAM_PORT 8080
#define DEFAULT_PROXY_CLIENT_TIMEOUT 5 /* seconds */
#define DEFAULT_PROXY_UPSTREAM_TIMEOUT 5 /* seconds */
#define DEFAULT_PROXY_NUM_WORKERS 0
#define PROXY_HOST_LEN 256
typedef struct _tcp_proxy_config
{
    char host[PROXY_HOST_LEN];
    char upstream_host[PROXY_HOST_LEN];
    int port;
    int upstream_port;
    int client_timeout; /* seconds, 0 for default */
    int upstream_timeout; /* seconds, 0 for default */
    int num_workers; /* 0 for default */
} tcp_proxy_config_t;

/* 
 * Allocate config in shared memory
 * Return -1 on error and 0 on success
 */
int alloc_config();

/*
 * Open read-only config
 * Return -1 on error and 0 on success
 */
int open_config();

void free_config();

/*
 * Set default config values. Assuming config already allocated
 */
void set_config_defaults();

/*
 * Get config filename
 */
const char *get_config_filename();

tcp_proxy_config_t *get_config();

/* 
 * Config initialization
 * Return -1 on error and 0 on success
 */
int parse_command_line(int argc, char **argv);
int parse_config_file(const char *filename);

/* debug */
void dump_config();
#endif /* TCP_PROXY_CONFIG_H_ */
