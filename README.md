# README #
This is a simple proxy for tcp protocol: listens for tcp connection on 1 address and than redirects all traffic from pre-defined host (set in config) to clients. The proxy uses [libuv](https://github.com/libuv/libuv) library for event-loop and multi-process paradigm to gain advantage from multi-core systems (by default the number of worker children is equal to number of cpu cores). 

### How do I get set up? ###

* First clone the repository: 

```
#!bash
git clone git@bitbucket.org:vleschuk/tcp_proxy.git
cd tcp_proxy
```
* build using cmake:

```
#!bash
mkdir build
cd build
cmake ..
make
```

* Running: to run proxy you can specify options in command-line:


```
#!bash
Usage: tcp_proxy [options]
options:
	-c file	-	path to config file (other options will be ignored)
	-h host	-	listen host address (default: 127.0.0.1)
	-p port	-	listen host port (default: 80)
	-H host	-	upstream host address (default: 127.0.0.1)
	-P port	-	upstream host port (default: 8080)
	-t seconds	-	client timeout (default: 5)
	-T seconds	-	upstream timeout (default: 5)
	-w integer	-	number of worker processes, 0 - num of cpus (default: 0)

```

or via config file (option -c), sample of config file is located in /sample subdir of the project.

* Dependencies: the project uses libuv and libconfig, however they are already included in project in 3rdparty dir. The only requirement is gcc compiler, autotools (to build libconfig and libuv) and cmake >= 2.8. NOTE: code uses gnu99 C extensions, thus compiler must support -std=gnu99.

* How to run tests: tests are located in /test subdirectory, however cmake will automatically copy them to your build directory. After successful build you can just run:
```
#!bash
prove
```

in build directory. Tests are written in perl and require the following modules: Test::More, LWP::UserAgent, HTTP::Server::Simple::CGI, Proc::ProcessTable. You can install them using cpan or your distro package manager.

# Tested on #
The software was tested with in the following environments:

* gcc-5.2.1, glibc-2.21 amd64

* gcc-4.7.3, glibc-2.21 amd64

* gcc-4.6.4, glibc-2.21 amd64

* gcc-4.2.2, glibc-2.9 i686

# TODO list #
  * Try to implement support for [linux tcp splice](http://www.haproxy.org/download/1.3/doc/tcp-splicing.txt) - this should significantly improve performance
  * Implement normal logging with loglevels and syslog support: currently it just spams to stderr
  * Daemonization
  * Watchdog functionality for main daemon: restart workers if any of them killed