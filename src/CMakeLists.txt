add_executable(tcp_proxy
    main.c 
    util.c
    config.c
)
target_link_libraries(tcp_proxy uv config rt)

add_executable(tcp_proxy_worker
    worker.c
    util.c
    config.c
)
target_link_libraries(tcp_proxy_worker uv config rt)

add_custom_command(TARGET tcp_proxy tcp_proxy_worker POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/test/testlib ${CMAKE_BINARY_DIR}/testlib
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/test/t ${CMAKE_BINARY_DIR}/t
)
