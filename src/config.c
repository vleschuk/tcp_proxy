#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <libconfig.h>

#include "defs.h"
#include "util.h"
#include "config.h"

static const char config_shm_name[] = "tcp_proxy_config";

static char config_file[PATH_MAX] = {0};

static int shm_fd = -1;

static void *config_ptr = NULL;

int alloc_config() {
    int res = 0; 
    shm_fd = shm_open(config_shm_name, O_CREAT|O_RDWR, 0666);
    if(-1 == shm_fd) {
        pr_err("Failed to create shared memory object: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

    if(-1 == ftruncate(shm_fd, sizeof(tcp_proxy_config_t))) {
        pr_err("Failed to set shared memory object size: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

    config_ptr = mmap(0, sizeof(tcp_proxy_config_t), PROT_READ|PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if(MAP_FAILED == config_ptr) {
        pr_err("Failed to map config object to shared memory: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

cleanup:
    if(-1 == res && -1 != shm_fd) {
        shm_unlink(config_shm_name);
    }
    return res;
}

int open_config() {
    int res = 0; 
    shm_fd = shm_open(config_shm_name, O_RDONLY, 0666);
    if(-1 == shm_fd) {
        pr_err("Failed to create shared memory object: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

    config_ptr = mmap(0, sizeof(tcp_proxy_config_t), PROT_READ, MAP_SHARED, shm_fd, 0);
    if(MAP_FAILED == config_ptr) {
        pr_err("Failed to map config object to shared memory: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

cleanup:
    if(-1 == res) {
        free_config();
    }
    return res;
}

void free_config() {
    if(NULL != config_ptr) {
        munmap(config_ptr, sizeof(tcp_proxy_config_t));
    }
    if(-1 != shm_fd) {
        shm_unlink(config_shm_name);
    }
}

tcp_proxy_config_t *get_config() {
    CHECK(NULL != config_ptr);
    return (tcp_proxy_config_t *)config_ptr;
}

const char *get_config_filename() {
    return config_file;
}

static void usage() {
    pr_err("Usage: %s [options]\n"
           "options:\n"
           "\t-c file\t-\tpath to config file (other options will be ignored)\n"
           "\t-h host\t-\tlisten host address (default: %s)\n"
           "\t-p port\t-\tlisten host port (default: %d)\n"
           "\t-H host\t-\tupstream host address (default: %s)\n"
           "\t-P port\t-\tupstream host port (default: %d)\n"
           "\t-t seconds\t-\tclient timeout (default: %d)\n"
           "\t-T seconds\t-\tupstream timeout (default: %d)\n"
           "\t-w integer\t-\tnumber of worker processes, 0 - num of cpus (default: %d)\n",
           "tcp_proxy",
           DEFAULT_PROXY_HOST, DEFAULT_PROXY_PORT,
           DEFAULT_PROXY_UPSTREAM_HOST, DEFAULT_PROXY_UPSTREAM_PORT,
           DEFAULT_PROXY_CLIENT_TIMEOUT, DEFAULT_PROXY_UPSTREAM_TIMEOUT,
           DEFAULT_PROXY_NUM_WORKERS);
}

void set_config_defaults() {
    tcp_proxy_config_t *cfg = get_config();
    CHECK(NULL != cfg);

    strncpy(cfg->host, DEFAULT_PROXY_HOST, PROXY_HOST_LEN);
    cfg->port = DEFAULT_PROXY_PORT;

    strncpy(cfg->upstream_host, DEFAULT_PROXY_UPSTREAM_HOST, PROXY_HOST_LEN);
    cfg->upstream_port = DEFAULT_PROXY_UPSTREAM_PORT;

    cfg->upstream_timeout = DEFAULT_PROXY_UPSTREAM_TIMEOUT;
    cfg->client_timeout = DEFAULT_PROXY_CLIENT_TIMEOUT;

    cfg->num_workers = DEFAULT_PROXY_NUM_WORKERS;
}

int parse_command_line(int argc, char **argv) {
    int opt = 0;

    set_config_defaults();
    tcp_proxy_config_t *cfg = get_config();
    ASSERT(NULL != cfg);

    while((opt = getopt(argc, argv, "c:h:p:H:P:t:T:w:")) != -1) {
        switch(opt) {
        case 'c':
            strncpy(config_file, optarg, PATH_MAX);
            /* config file found, nothing to do here further */
            return 0;
        case 'h':
            strncpy(cfg->host, optarg, PROXY_HOST_LEN);
            break;
        case 'p':
            cfg->port = atoi(optarg);
            break;
        case 'H':
            strncpy(cfg->upstream_host, optarg, PROXY_HOST_LEN);
            break;
        case 'P':
            cfg->upstream_port = atoi(optarg);
            break;
        case 't':
            cfg->client_timeout = atoi(optarg);
            break;
        case 'T':
            cfg->upstream_timeout = atoi(optarg);
            break;
        case 'w':
            cfg->num_workers = atoi(optarg);
            break;
        default:
            usage();
            return -1;
        }
    }

    return 0;
}

int parse_config_file(const char *filename) {
    int res = 0;
    config_t cfg;
    const char *str;

    tcp_proxy_config_t *proxy_config = get_config();
    ASSERT(NULL != proxy_config);

    config_init(&cfg);

    if(!config_read_file(&cfg, filename)) {
        pr_err("%s:%d - %s\n", config_error_file(&cfg),
            config_error_line(&cfg), config_error_text(&cfg));
        res = -1;
        goto cleanup;
    }

    set_config_defaults();

    if(CONFIG_FALSE == config_lookup_string(&cfg, "host", &str)) {
        pr_debug("host param not found, using default: '%s'", DEFAULT_PROXY_HOST);
    }
    else {
        strncpy(proxy_config->host, str, PROXY_HOST_LEN);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "port", &proxy_config->port)) {
        pr_debug("port param not found, using default: %d", DEFAULT_PROXY_PORT);
    }

    if(CONFIG_FALSE == config_lookup_string(&cfg, "upstream_host", &str)) {
        pr_debug("upstream_host param not found, using default: '%s'", DEFAULT_PROXY_UPSTREAM_HOST);
    }
    else {
        strncpy(proxy_config->upstream_host, str, PROXY_HOST_LEN);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "upstream_port", &proxy_config->upstream_port)) {
        pr_debug("upstream_port param not found, using default: %d", DEFAULT_PROXY_UPSTREAM_PORT);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "upstream_timeout", &proxy_config->upstream_timeout)) {
        pr_debug("upstream_timeout param not found, using default: %d", DEFAULT_PROXY_UPSTREAM_TIMEOUT);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "client_timeout", &proxy_config->client_timeout)) {
        pr_debug("client_timeout param not found, using default: %d", DEFAULT_PROXY_CLIENT_TIMEOUT);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "workers", &proxy_config->num_workers)) {
        pr_debug("workers param not found, using default: %d", DEFAULT_PROXY_NUM_WORKERS);
    }

cleanup:
    config_destroy(&cfg);
    return res;
}

void dump_config() {
    const tcp_proxy_config_t *config = get_config();
    if(!config) {
        return;
    }
    pr_debug("\nConfiguration:\nhost: %s port: %d\n"
             "upstream_host: %s upstream_port: %d\n"
             "upstream_timeout: %d client_timeout: %d\n"
             "workers: %d", config->host, config->port, config->upstream_host, config->upstream_port,
             config->upstream_timeout, config->client_timeout, config->num_workers);
}
