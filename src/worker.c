#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>

#include "defs.h"
#include "util.h"
#include "config.h"

/* A connection is modeled as an abstraction on top of two simple state
 * machines, one for reading and one for writing.  Either state machine
 * is, when active, in one of three states: busy, done or stop; the fourth
 * and final state, dead, is an end state and only relevant when shutting
 * down the connection.  A short overview:
 *
 *                          busy                  done           stop
 *  ----------|---------------------------|--------------------|------|
 *  readable  | waiting for incoming data | have incoming data | idle |
 *  writable  | busy writing out data     | completed write    | idle |
 *
 * We could remove the done state from the writable state machine. For our
 * purposes, it's functionally equivalent to the stop state.
 *
 * When the connection with upstream has been established, the client_ctx
 * moves into a state where incoming data from the client is sent upstream
 * and vice versa, incoming data from upstream is sent to the client.  In
 * other words, we're just piping data back and forth.  See conn_cycle()
 * for details.
 *
 * An interesting deviation from libuv's I/O model is that reads are discrete
 * rather than continuous events.  In layman's terms, when a read operation
 * completes, the connection stops reading until further notice.
 *
 * The rationale for this approach is that we have to wait until the data
 * has been sent out again before we can reuse the read buffer.
 *
 * It also pleasingly unifies with the request model that libuv uses for
 * writes and everything else; libuv may switch to a request model for
 * reads in the future.
 */
enum conn_state {
    c_busy,  /* Busy; waiting for incoming data or for a write to complete. */
    c_done,  /* Done; read incoming data or write finished. */
    c_stop,  /* Stopped. */
    c_dead
};

/* Session states. */
enum sess_state {
    s_req_start,        /* Start waiting for request data. */
    s_req_lookup,       /* Wait for upstream hostname DNS lookup to complete. */
    s_req_connect,      /* Wait for uv_tcp_connect() to complete. */
    s_proxy_start,      /* Connected. Start piping data. */
    s_proxy,            /* Connected. Pipe data back and forth. */
    s_kill,             /* Tear down session. */
    s_almost_dead_0,    /* Waiting for finalizers to complete. */
    s_almost_dead_1,    /* Waiting for finalizers to complete. */
    s_almost_dead_2,    /* Waiting for finalizers to complete. */
    s_almost_dead_3,    /* Waiting for finalizers to complete. */
    s_almost_dead_4,    /* Waiting for finalizers to complete. */
    s_dead              /* Dead. Safe to free now. */
};

static const char *progname = __FILE__; /* reset in main() */
static const tcp_proxy_config_t *cfg = NULL;

static uv_loop_t *loop;
static uv_pipe_t queue;

static void on_new_connection(uv_stream_t *q, ssize_t nread, const uv_buf_t *buf);
static void do_next(client_ctx *cx);
static int do_req_start(client_ctx *cx);
static int do_req_lookup(client_ctx *cx);
static int do_req_connect_start(client_ctx *cx);
static int do_req_connect(client_ctx *cx);
static int do_proxy_start(client_ctx *cx);
static int do_proxy(client_ctx *cx);
static int do_kill(client_ctx *cx);
static int do_almost_dead(client_ctx *cx);
static int conn_cycle(const char *who, conn *a, conn *b);
static void conn_timer_reset(conn *c);
static void conn_timer_expire(uv_timer_t *handle);
static int conn_connect(conn *c);
static void conn_connect_done(uv_connect_t *req, int status);
static void conn_read(conn *c);
static void conn_read_done(uv_stream_t *handle,
                           ssize_t nread,
                           const uv_buf_t *buf);
static void pipe_alloc(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf);
static void conn_alloc(uv_handle_t *handle, size_t size, uv_buf_t *buf);
static void conn_write(conn *c, const void *data, unsigned int len);
static void conn_write_done(uv_write_t *req, int status);
static void conn_close(conn *c);
static void conn_close_done(uv_handle_t *handle);
static void handle_stop(uv_signal_t *handle, int signum);
static void init_signals();
static void cleanup();

int main(int argc, char **argv) {

    progname = basename(argv[0]);

    if(-1 == open_config()) {
        pr_err("Failed to open configuration");
        exit(EXIT_FAILURE);
    }

    if(0 != atexit(cleanup)) {
        pr_err("Can't register cleanup function");
        exit(EXIT_FAILURE);
    }

    cfg = get_config();
    loop = uv_default_loop();

    init_signals();

    uv_pipe_init(loop, &queue, 1 /* ipc */);
    int err = uv_pipe_open(&queue, 0);
    if(err) {
        pr_err("Failed to open pipe with parent: %s", uv_err_name(err));
        exit(EXIT_FAILURE);
    }
    uv_read_start((uv_stream_t*)&queue, pipe_alloc, on_new_connection);
    return uv_run(loop, UV_RUN_DEFAULT);
}

void on_new_connection(uv_stream_t *q, ssize_t nread, const uv_buf_t *buf) {
    pr_debug(__func__);
    if (nread < 0) {
        if (nread != UV_EOF) {
            pr_err("Read error %s\n", uv_err_name(nread));
        }
        uv_close((uv_handle_t*) q, NULL);
        return;
    }

    uv_pipe_t *pipe = (uv_pipe_t*) q;
    if (!uv_pipe_pending_count(pipe)) {
        pr_err("No pending count\n");
        return;
    }

    uv_handle_type pending = uv_pipe_pending_type(pipe);
    assert(pending == UV_TCP);

    client_ctx *cx = xmalloc(sizeof(*cx));
    conn *incoming;
    conn *outgoing;

    pr_debug("Initiating tcp connection");
    CHECK(0 == uv_tcp_init(loop, &cx->incoming.handle.tcp));
    CHECK(0 == uv_accept(q, &cx->incoming.handle.stream));
    uv_os_fd_t fd;
    uv_fileno((const uv_handle_t*)&cx->incoming.handle.handle, &fd);
    pr_info("Worker %d: Accepted fd %d\n", getpid(), fd);
    cx->state = s_req_start;

    incoming = &cx->incoming;
    incoming->client = cx;
    incoming->result = 0;
    incoming->rdstate = c_stop;
    incoming->wrstate = c_stop;
    incoming->idle_timeout = cfg->client_timeout * 1000;
    CHECK(0 == uv_timer_init(loop, &incoming->timer_handle));

    outgoing = &cx->outgoing;
    outgoing->client = cx;
    outgoing->result = 0;
    outgoing->rdstate = c_stop;
    outgoing->wrstate = c_stop;
    outgoing->idle_timeout = cfg->upstream_timeout * 1000;
    CHECK(0 == uv_tcp_init(loop, &outgoing->handle.tcp));
    CHECK(0 == uv_timer_init(loop, &outgoing->timer_handle));

    do_next(cx);
}

/* 
 * This is the core state machine that drives the client <-> upstream proxy.
 */
static void do_next(client_ctx *cx) {
    pr_debug("%s state: %d", __func__, cx->state);
    int new_state;

    ASSERT(cx->state != s_dead);
    switch (cx->state) {
      case s_req_start:
        new_state = do_req_start(cx);
        break;
      case s_req_lookup:
        new_state = do_req_lookup(cx);
        break;
      case s_req_connect:
        new_state = do_req_connect(cx);
        break;
      case s_proxy_start:
        new_state = do_proxy_start(cx);
        break;
      case s_proxy:
        new_state = do_proxy(cx);
        break;
      case s_kill:
        new_state = do_kill(cx);
        break;
      case s_almost_dead_0:
      case s_almost_dead_1:
      case s_almost_dead_2:
      case s_almost_dead_3:
      case s_almost_dead_4:
        new_state = do_almost_dead(cx);
        break;
      default:
        pr_err("invalid state: %d", cx->state);
        UNREACHABLE();
    }
    cx->state = new_state;

    if (cx->state == s_dead) {
      free(cx);
    }
}

static int do_req_start(client_ctx *cx) {
    pr_debug(__func__);
    conn *incoming;

    incoming = &cx->incoming;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_stop);
    incoming->wrstate = c_stop;

    if (incoming->result < 0) {
      pr_err("write error: %s", uv_strerror(incoming->result));
      return do_kill(cx);
    }

    /* conn_read(incoming); */
    return do_req_lookup(cx);
}

static int do_req_lookup(client_ctx *cx) {
    pr_debug(__func__);
    conn *incoming;
    conn *outgoing;

    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_stop);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);

    memset(&outgoing->t.addr4, 0, sizeof(outgoing->t.addr4));
    outgoing->t.addr4.sin_family = AF_INET;
    outgoing->t.addr4.sin_port = htons(cfg->upstream_port);
    inet_aton(cfg->upstream_host, &outgoing->t.addr4.sin_addr);
    return do_req_connect_start(cx);
}

/* Assumes that cx->outgoing.t.sa contains a valid AF_INET/AF_INET6 address. */
static int do_req_connect_start(client_ctx *cx) {
    pr_debug(__func__);
    conn *incoming;
    conn *outgoing;
    int err;

    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_stop);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);

    err = conn_connect(outgoing);
    if (err != 0) {
        pr_err("connect error: %s\n", uv_strerror(err));
        return do_kill(cx);
    }

    return s_req_connect;
}

static int do_req_connect(client_ctx *cx) {
    pr_debug(__func__);
    conn *incoming;
    conn *outgoing;

    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_stop);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);

    if (outgoing->result == 0) {
        return s_proxy_start;
    } else {
        pr_err("upstream connection error: %s\n", uv_strerror(outgoing->result));
        return s_kill;
    }

    UNREACHABLE();
    return s_kill;
}

static int do_proxy_start(client_ctx *cx) {
    pr_debug(__func__);
    conn *incoming;
    conn *outgoing;

    incoming = &cx->incoming;
    outgoing = &cx->outgoing;
    ASSERT(incoming->rdstate == c_stop);
    ASSERT(incoming->wrstate == c_stop);
    ASSERT(outgoing->rdstate == c_stop);
    ASSERT(outgoing->wrstate == c_stop);
    incoming->wrstate = c_stop;

    if (incoming->result < 0) {
        pr_err("write error: %s", uv_strerror(incoming->result));
        return do_kill(cx);
    }

    conn_read(incoming);
    conn_read(outgoing);
    return s_proxy;
}

/* Proxy incoming data back and forth. */
static int do_proxy(client_ctx *cx) {
    pr_debug(__func__);
    if (conn_cycle("client", &cx->incoming, &cx->outgoing)) {
        return do_kill(cx);
    }

    if (conn_cycle("upstream", &cx->outgoing, &cx->incoming)) {
        return do_kill(cx);
    }

    return s_proxy;
}

static int do_kill(client_ctx *cx) {
    pr_debug(__func__);
    int new_state;

    if (cx->state >= s_almost_dead_0) {
        return cx->state;
    }

    /* Try to cancel the request. The callback still runs but if the
     * cancellation succeeded, it gets called with status=UV_ECANCELED.
     */
    new_state = s_almost_dead_1;
    if (cx->state == s_req_lookup) {
        new_state = s_almost_dead_0;
        pr_debug("%s: trying to cancel request", __func__);
        uv_cancel(&cx->outgoing.t.req);
    }

    conn_close(&cx->incoming);
    conn_close(&cx->outgoing);
    return new_state;
}

static int do_almost_dead(client_ctx *cx) {
    pr_debug("%s state: %d", __func__, cx->state);
    ASSERT(cx->state >= s_almost_dead_0);
    int ret = cx->state + 1;
    pr_debug("%s returning state: %d", __func__, ret);
    return ret;  /* Another finalizer completed. */
}

static int conn_cycle(const char *who, conn *a, conn *b) {
    pr_debug(__func__);
    if (a->result < 0) {
        if (a->result != UV_EOF) {
          pr_err("%s error: %s", who, uv_strerror(a->result));
        }
        return -1;
    }

    if (b->result < 0) {
        return -1;
    }

    if (a->wrstate == c_done) {
        a->wrstate = c_stop;
    }

    /* The logic is as follows: read when we don't write and write when we don't
     * read.  That gives us back-pressure handling for free because if the peer
     * sends data faster than we consume it, TCP congestion control kicks in.
     */
    if (a->wrstate == c_stop) {
        if (b->rdstate == c_stop) {
            conn_read(b);
        } else if (b->rdstate == c_done) {
            conn_write(a, b->t.buf, b->result);
            b->rdstate = c_stop;  /* Triggers the call to conn_read() above. */
        }
    }

    return 0;
}

static void conn_timer_reset(conn *c) {
    pr_debug(__func__);
    CHECK(0 == uv_timer_start(&c->timer_handle,
                              conn_timer_expire,
                              c->idle_timeout,
                              0));
}

static void conn_timer_expire(uv_timer_t *handle) {
    pr_debug(__func__);
    conn *c;

    c = CONTAINER_OF(handle, conn, timer_handle);
    c->result = UV_ETIMEDOUT;
    do_next(c->client);
}

static int conn_connect(conn *c) {
    pr_debug(__func__);
    conn_timer_reset(c);
    return uv_tcp_connect(&c->t.connect_req,
                          &c->handle.tcp,
                          &c->t.addr,
                          conn_connect_done);
}

static void conn_connect_done(uv_connect_t *req, int status) {
    pr_debug(__func__);
    conn *c;

    if (status == UV_ECANCELED) {
        return;  /* Handle has been closed. */
    }

    c = CONTAINER_OF(req, conn, t.connect_req);
    c->result = status;
    c->client->state = s_proxy_start;
    do_next(c->client);
}

static void conn_read(conn *c) {
    pr_debug(__func__);
    ASSERT(c->rdstate == c_stop);
    CHECK(0 == uv_read_start(&c->handle.stream, conn_alloc, conn_read_done));
    c->rdstate = c_busy;
    conn_timer_reset(c);
}

static void conn_read_done(uv_stream_t *handle,
                             ssize_t nread,
                             const uv_buf_t *buf) {
    pr_debug(__func__);
    conn *c;

    c = CONTAINER_OF(handle, conn, handle);
    ASSERT(c->t.buf == buf->base);
    ASSERT(c->rdstate == c_busy);
    c->rdstate = c_done;
    c->result = nread;

    uv_read_stop(&c->handle.stream);
    do_next(c->client);
}

void pipe_alloc(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf) {
    buf->base = malloc(suggested_size);
    buf->len = suggested_size;
}

static void conn_alloc(uv_handle_t *handle, size_t size, uv_buf_t *buf) {
    pr_debug(__func__);
    conn *c;

    c = CONTAINER_OF(handle, conn, handle);
    ASSERT(c->rdstate == c_busy);
    buf->base = c->t.buf;
    buf->len = sizeof(c->t.buf);
}

static void conn_write(conn *c, const void *data, unsigned int len) {
    pr_debug(__func__);
    uv_buf_t buf;

    ASSERT(c->wrstate == c_stop || c->wrstate == c_done);
    c->wrstate = c_busy;

    /* It's okay to cast away constness here, uv_write() won't modify the
     * memory.
     */
    buf.base = (char *) data;
    buf.len = len;

    CHECK(0 == uv_write(&c->write_req,
                        &c->handle.stream,
                        &buf,
                        1,
                        conn_write_done));
    conn_timer_reset(c);
}

static void conn_write_done(uv_write_t *req, int status) {
    pr_debug(__func__);
    conn *c;

    if (status == UV_ECANCELED) {
        return;  /* Handle has been closed. */
    }

    c = CONTAINER_OF(req, conn, write_req);
    ASSERT(c->wrstate == c_busy);
    c->wrstate = c_done;
    c->result = status;
    do_next(c->client);
}

static void conn_close(conn *c) {
    pr_debug(__func__);
    ASSERT(c->rdstate != c_dead);
    ASSERT(c->wrstate != c_dead);
    c->rdstate = c_dead;
    c->wrstate = c_dead;
    c->timer_handle.data = c;
    c->handle.handle.data = c;
    uv_close(&c->handle.handle, conn_close_done);
    uv_close((uv_handle_t *) &c->timer_handle, conn_close_done);
}

static void conn_close_done(uv_handle_t *handle) {
    conn *c;

    c = handle->data;
    pr_debug("%s: state: %d", __func__, c->client->state);
    do_next(c->client);
}

static void handle_stop(uv_signal_t *handle, int signum) {
    pr_info("Received signal: %d", signum);
    uv_stop(loop);
}

static void init_signals() {
    static uv_signal_t sigint, sigterm, sigquit;

    uv_signal_init(loop, &sigint);
    uv_signal_start(&sigint, handle_stop, SIGINT);

    uv_signal_init(loop, &sigterm);
    uv_signal_start(&sigterm, handle_stop, SIGTERM);

    uv_signal_init(loop, &sigquit);
    uv_signal_start(&sigquit, handle_stop, SIGQUIT);
}

static void cleanup() {
    pr_debug("Cleaning up");
    free_config();
}

const char *get_prog_name() {
    return progname;
}

