#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uv.h>
#include <libgen.h>

#include "util.h"
#include "config.h"

static const char *progname = __FILE__; /* reset in main() */

static const tcp_proxy_config_t *cfg = NULL;
static uv_loop_t *loop;

struct child_worker {
    uv_process_t req;
    uv_process_options_t options;
    uv_pipe_t pipe;
} *workers;

static int round_robin_counter;
static int child_worker_count;

static uv_buf_t dummy_buf;
static char worker_path[500];
static const char manager_bin_name[] = "tcp_proxy";
static const char worker_bin_name[] = "tcp_proxy_worker";

static void close_process_handle(uv_process_t *req, int64_t exit_status, int term_signal);
static void on_new_connection(uv_stream_t *server, int status);
static void handle_stop(uv_signal_t *handle, int signum);
static void init_signals();
static void setup_workers();
static void stop_workers();
static void prepare_config(int argc, char **argv);
static void cleanup();

int main(int argc, char **argv) {

    progname = basename(argv[0]);

    if(0 != atexit(cleanup)) {
        pr_err("Can't register cleanup function");
        exit(EXIT_FAILURE);
    }

    prepare_config(argc, argv);
    dump_config();
    loop = uv_default_loop();

    init_signals();

    uv_tcp_t server;
    uv_tcp_init(loop, &server);

    struct sockaddr_in bind_addr;
    uv_ip4_addr(cfg->host, cfg->port, &bind_addr);

    int r;
    if ((r = uv_tcp_bind(&server, (const struct sockaddr *)&bind_addr, 0))) {
        pr_err("Bind error: %s", uv_err_name(r));
        exit(EXIT_FAILURE);
    }

    if ((r = uv_listen((uv_stream_t*) &server, 128, on_new_connection))) {
        pr_err("Listen error: %s", uv_err_name(r));
        exit(EXIT_FAILURE);
    }

    setup_workers();

    uv_run(loop, UV_RUN_DEFAULT);
    exit(EXIT_SUCCESS);
}

static void close_process_handle(uv_process_t *req, int64_t exit_status, int term_signal) {
    pr_info("Process exited with status %" PRId64 ", signal %d", exit_status, term_signal);
    uv_close((uv_handle_t*) req, NULL);
}

static void on_new_connection(uv_stream_t *server, int status) {
    if (status == -1) {
        // error!
        return;
    }

    uv_tcp_t *client = (uv_tcp_t*) malloc(sizeof(uv_tcp_t));
    uv_tcp_init(loop, client);
    if (uv_accept(server, (uv_stream_t*) client) == 0) {
        uv_write_t *write_req = (uv_write_t*) malloc(sizeof(uv_write_t));
        dummy_buf = uv_buf_init("a", 1);
        struct child_worker *worker = &workers[round_robin_counter];
        uv_write2(write_req, (uv_stream_t*) &worker->pipe, &dummy_buf, 1, (uv_stream_t*) client, NULL);
        round_robin_counter = (round_robin_counter + 1) % child_worker_count;
    }
    uv_close((uv_handle_t*) client, NULL);
}

static void setup_workers() {
    size_t path_size = 500;
    uv_exepath(worker_path, &path_size);
    strcpy(worker_path + (strlen(worker_path) - sizeof(manager_bin_name) + 1), worker_bin_name);
    pr_info("Worker path: %s", worker_path);

    char* args[2];
    args[0] = worker_path;
    args[1] = NULL;

    round_robin_counter = 0;

    child_worker_count = cfg->num_workers;

    if(0 == child_worker_count) {
        // launch same number of workers as number of CPUs
        uv_cpu_info_t *info;
        int cpu_count;
        uv_cpu_info(&info, &cpu_count);
        uv_free_cpu_info(info, cpu_count);

        child_worker_count = cpu_count;
    }

    pr_info("Set number of workers to %d", child_worker_count);

    workers = calloc(sizeof(struct child_worker), child_worker_count);
    int i = child_worker_count;
    while (i--) {
        struct child_worker *worker = &workers[i];
        uv_pipe_init(loop, &worker->pipe, 1);

        uv_stdio_container_t child_stdio[3];
        child_stdio[0].flags = UV_CREATE_PIPE | UV_READABLE_PIPE;
        child_stdio[0].data.stream = (uv_stream_t*) &worker->pipe;
        child_stdio[1].flags = UV_IGNORE;
        child_stdio[2].flags = UV_INHERIT_FD;
        child_stdio[2].data.fd = 2;

        worker->options.stdio = child_stdio;
        worker->options.stdio_count = 3;

        worker->options.exit_cb = close_process_handle;
        worker->options.file = args[0];
        worker->options.args = args;

        uv_spawn(loop, &worker->req, &worker->options); 
        pr_info("Started worker %d", worker->req.pid);
    }
}

static void stop_workers() {
    pr_debug("Stopping worker processes");
    for(int i = 0; i < child_worker_count; ++i) {
        uv_process_kill(&workers[i].req, SIGTERM);
        /* nothing to do here: worker exit_cb will take care of handle */
    }
}

static void prepare_config(int argc, char **argv) {
    if(-1 == alloc_config()) {
        pr_err("Can't initialize configuration");
        exit(EXIT_FAILURE);
    }
    cfg = get_config();

    if(argc == 1) {
        set_config_defaults();
        return;
    }

    if(argc > 1 && -1 == parse_command_line(argc, argv)) {
        pr_err("Failed to parse command line");
        exit(EXIT_FAILURE);
    }

    if('\0' != *get_config_filename() && -1 == parse_config_file(get_config_filename())) {
        pr_err("Can't parse config file");
        exit(EXIT_FAILURE);
    }
}

static void handle_stop(uv_signal_t *handle, int signum) {
    pr_info("Received signal: %d", signum);
    stop_workers();
    uv_stop(loop);
}

static void init_signals() {
    static uv_signal_t sigint, sigterm, sigquit;

    uv_signal_init(loop, &sigint);
    uv_signal_start(&sigint, handle_stop, SIGINT);

    uv_signal_init(loop, &sigterm);
    uv_signal_start(&sigterm, handle_stop, SIGTERM);

    uv_signal_init(loop, &sigquit);
    uv_signal_start(&sigquit, handle_stop, SIGQUIT);
}

static void cleanup() {
    pr_debug("Cleaning up");
    free_config();
}

const char *get_prog_name() {
    return progname;
}
